// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef PARTICLE_FUNCTION_HPP
#define PARTICLE_FUNCTION_HPP

#include <functional>

#include <particle.hpp>
#include <vector.hpp>

namespace SPH
{
    // Alias for scalar function, templated on problem dimension
    template <unsigned N>
    using ScalarFunctionND = std::function<double (
            const ParticleData<N>& data)>;

    // Alias for vector function, templated on problem dimension
    template <unsigned N>
    using VectorFunctionND = std::function<VectorND<N, double> (
            const ParticleData<N>& data)>;

    // Aliases for up to three dimensions
    using ScalarFunction1D = ScalarFunctionND<1>;
    using ScalarFunction2D = ScalarFunctionND<2>;
    using ScalarFunction3D = ScalarFunctionND<3>;
    using VectorFunction1D = VectorFunctionND<1>;
    using VectorFunction2D = VectorFunctionND<2>;
    using VectorFunction3D = VectorFunctionND<3>;
}   // namespace SPH

#endif  // PARTICLE_FUNCTION_HPP

