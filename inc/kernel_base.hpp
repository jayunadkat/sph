// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef KERNEL_BASE_HPP
#define KERNEL_BASE_HPP

namespace SPH
{
    // Interface for kernel object, templated on problem dimension
    template <unsigned N>
    class KernelND
    {
        protected:
            // Scaling for support of kernel based on particle separation
            double m_alpha = 1.3;
            // Support of kernel
            double m_h = 0.;

        public:
            virtual ~KernelND() = default;

            // Get/set kernel support data
            const double& alpha() const;
            void set_alpha(double alpha);
            const double& h() const;
            void update_h(double dp);

            // Kernel and kernel gradient functions
            virtual double W(const VectorND<N, double>& r) const = 0;
            virtual VectorND<N, double> GradW(
                    const VectorND<N, double>& r) const = 0;
    };

#include <kernel_base.impl>
}   // namespace SPH

#endif  // KERNEL_BASE_HPP

