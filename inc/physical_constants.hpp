// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef PHYSICAL_CONSTANTS_HPP
#define PHYSICAL_CONSTANTS_HPP

#include <vector.hpp>

namespace SPH
{
    // Material properties
    namespace Material
    {
        namespace Water
        {
            const double density = 1000.;
            const double c_p = 4186.;
            const double k = 0.6;
        }   // namespace Water

        namespace Steel
        {
            const double density = 8050;
            const double c_p = 490.;
            const double k = 50.2;
        }
    }   // namespace Material

    // Variables governing physics
    namespace Physics
    {
        const double gravitational_acceleration = 9.81;
        template <unsigned N>
        const VectorND<N, double> gravityND;
        template <>
        const VectorND<2, double> gravityND<2> =
                {0., -gravitational_acceleration};
        template <>
        const VectorND<3, double> gravityND<3> =
                {0., 0., -gravitational_acceleration};
        const double polytropic_index = 7.;
    }   // namespace Physics

    // Problem-specific physical variables such as fluid box boundaries or fluid
    // height. Also problem-specific computation-related variables such as CFL
    // number.
    namespace Problem
    {
        const double fluid_height = 0.6;
        const double reference_density = Material::Steel::density;
        const double CFL = 0.2;
    }   // namespace Problem
}   // namespace SPH

#endif  // PHYSICAL_CONSTANTS_HPP

