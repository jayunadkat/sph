#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <cmath>
#include <initializer_list>
#include <ostream>
#include <stdexcept>
#include <vector>

namespace SPH
{
    // Vector class containing a std::vector, templated on dimension and type
    template <unsigned N, typename T>
    class VectorND
    {
        protected:
            std::vector<T> m_data = std::vector<T>(N, T(0));

        public:
            VectorND() = default;
            // Store data directly from specified initializer_list (allows us
            // to create vectors from brace-enclosed list of data)
            VectorND(std::initializer_list<T> list);

            // Get/set data
            T operator[](const std::size_t& i) const;
            T& operator[](const std::size_t& i);
            // Clear all data
            void zero();

            // Arithmetic operators
            VectorND& operator+=(const VectorND& rhs);
            VectorND operator+(const VectorND& rhs) const;

            VectorND& operator-=(const VectorND& rhs);
            VectorND operator-(const VectorND& rhs) const;

            VectorND& operator*=(const T& rhs);
            VectorND operator*(const T& rhs) const;
            T operator*(const VectorND& rhs) const;
            friend VectorND operator*(const T& lhs, const VectorND& rhs)
            {
                return rhs*lhs;
            }

            VectorND& operator/=(const T& rhs);
            VectorND operator/(const T& rhs) const;

            // Lp norms
            T L0_norm() const;
            T L1_norm() const;
            T L2_norm() const;

            // Returns vector of the appropriate dimension for which each
            // component is unity
            static VectorND ones();

            // Stream output
            friend std::ostream& operator<<(std::ostream& out,
                    const VectorND& rhs)
            {
                for (auto&& x : rhs.m_data)
                    out << x << ' ';
                return out;
            }
    };

#include <vector.impl>

    // Aliases for up to three dimensions
    using Vector1f = VectorND<1, double>;
    using Vector1u = VectorND<1, unsigned>;
    using Vector2f = VectorND<2, double>;
    using Vector2u = VectorND<2, unsigned>;
    using Vector3f = VectorND<3, double>;
    using Vector3u = VectorND<3, unsigned>;
}   // namespace SPH

#endif  // VECTOR_HPP

