// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef EQUATION_OF_STATE_HPP
#define EQUATION_OF_STATE_HPP

#include <cmath>

#include <physical_constants.hpp>

namespace SPH
{
    namespace EquationOfState
    {
        // Equation of state object should implement member functions returning
        // pressure for given particle data, sound speed, and particle sound
        // speed. Global and problem physical variables are available in
        // physical_constants.hpp

        // Implementation of Tait's equation of state
        template <unsigned N>
        struct TaitND
        {
            double pressure(const ParticleData<N>& data) const;
            double sound_speed() const;
            double particle_sound_speed(const ParticleData<N>& data) const;
        };

#include <equation_of_state.impl>

        // Aliases for up to three dimensions
        using Tait1D = TaitND<1>;
        using Tait2D = TaitND<2>;
        using Tait3D = TaitND<3>;
    }   // namespace EquationOfState
}   // namespace SPH

#endif  // EQUATION_OF_STATE_HPP

