// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef QUAD_TREE_HPP
#define QUAD_TREE_HPP

#include <algorithm>
#include <iostream>
#include <ostream>
#include <vector>

#include <vector.hpp>

namespace SPH
{
    // Represents axis-aligned rectangle and provides member functions to
    // determine whether a given point is contained in the rectangle or
    // if two given rectangles intersect. Implemented using corners instead of
    // centre/half-sizes to avoid any floating-point operations in bounds
    // checks. This ensures that areas are conserved in QuadTree::subdivide
    struct Rectangle
    {
        Vector2f corner1;
        Vector2f corner2;

        bool contains(const Vector2f& r) const;
        bool intersects(const Rectangle& rect) const;
    };

    // Quad tree for spatial partitioning of particles, for computation
    // efficiency
    template <typename T>
    class QuadTree
    {
        protected:
            // Capacity of each leaf of the tree
            static const unsigned capacity = 10;

            // Pointers to sub-trees, and bounds of this quad
            QuadTree* m_nw = nullptr;
            QuadTree* m_ne = nullptr;
            QuadTree* m_se = nullptr;
            QuadTree* m_sw = nullptr;
            Rectangle m_bounds;

            // Items contained in this tree (should only be non-empty if this
            // tree is a leaf)
            std::vector<T*> mp_items;

        public:
            QuadTree() = default;
            QuadTree(Rectangle&& bounds);
            ~QuadTree();

            // Recursively remove particles from this tree and its children
            void dispose();
            // Clear tree and all children, then re-initialise bounds for this
            // tree and insert all specified particles
            void build(std::vector<T>& items);
            // Insert specified particle into this tree
            bool insert(T& item);
            // Divide this tree into four sub-trees and distribute items to the
            // new leaves
            void subdivide();
            // Obtain pointers to all particles falling in the specified
            // rectangle
            std::vector<T*> query(const Rectangle& range) const;

            // Stream output, specifying corners of tree and sub-trees
            // recursively
            friend std::ostream& operator<<(std::ostream& out,
                    const QuadTree& rhs)
            {
                if (rhs.m_nw == nullptr)
                {
                    const Rectangle& b(rhs.m_bounds);
                    out << b.corner1 << std::endl
                        << Vector2f{b.corner1[0], b.corner2[1]} << std::endl
                        << b.corner2 << std::endl
                        << Vector2f{b.corner2[0], b.corner1[1]} << std::endl
                        << b.corner1 << std::endl << std::endl;
                }
                else
                    out << *rhs.m_nw << *rhs.m_ne << *rhs.m_se << *rhs.m_sw;

                return out;
            }
    };

#include <quad_tree.impl>
}   // namespace SPH

#endif  // QUAD_TREE_HPP

