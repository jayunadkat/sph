// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include <ostream>

#include <vector.hpp>

namespace SPH
{
    // Struct to hold rate of change information for flow variables
    template <unsigned N>
    struct TimestepData
    {
        VectorND<N, double> dudt;
        double drhodt = 0.;
        double dTdt = 0.;
    };

    // Struct to hold particle data: position, velocity, mass, density, and
    // pressure
    template <unsigned N>
    struct ParticleData
    {
        VectorND<N, double> position;
        VectorND<N, double> velocity;
        double mass = 0.;
        double density = 0.;
        double pressure = 0.;
        double temperature = 0.;
    };

    // Particle contains current and previous physical data as well as rates of
    // change of flow variables. Flags are available to specify whether
    // particles are to move or not, and whether they should interact with
    // other particles or not.
    template <unsigned N>
    class ParticleND
    {
        protected:
            // Rates of change of flow variables
            TimestepData<N> m_timestep_data;
            // Previous and current physical data
            ParticleData<N> m_previous_data;
            ParticleData<N> m_current_data;
            // Flag to denote stationary particle
            bool m_pinned = false;
            // Flag to denote non-interacting particle
            bool m_ghost = false;

        public:
            // Get/set physical data
            const ParticleData<N>& particle_data() const;
            const VectorND<N, double>& r() const;
            VectorND<N, double>& r();
            const VectorND<N, double>& u() const;
            VectorND<N, double>& u();
            const double& m() const;
            double& m();
            const double& rho() const;
            double& rho();
            const double& p() const;
            double& p();
            const double& T() const;
            double& T();

            // Get/set flags
            bool pinned() const;
            void set_pinned(bool pinned = true);
            bool ghost() const;
            void set_ghost(bool ghost = true);

            // Store rates of change for this particle
            void update_timestep_data(const TimestepData<N>& timestep_data);
            // Perform time step
            void step(double dt);
            // Undo last time step
            void undo_step();

            // Stream output, dump location, velocity, mass, density, and
            // pressure for particle
            friend std::ostream& operator<<(std::ostream& out,
                    const ParticleND& rhs)
            {
                return out << rhs.r() << rhs.u() << rhs.m() << ' ' << rhs.rho()
                    << ' ' << rhs.p() << ' ' << rhs.T();
            }
    };

#include <particle.impl>

    // Aliases for up to three dimensions
    using ParticleData1D = ParticleData<1>;
    using ParticleData2D = ParticleData<2>;
    using ParticleData3D = ParticleData<3>;
    using Particle1D = ParticleND<1>;
    using Particle2D = ParticleND<2>;
    using Particle3D = ParticleND<3>;
}   // namespace SPH

#endif  // PARTICLE_HPP

