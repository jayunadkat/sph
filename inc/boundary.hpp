// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef BOUNDARY_HPP
#define BOUNDARY_HPP

#include <set>

#include <particle.hpp>

namespace SPH
{
    // Stores a collection of pointers to particles considered to be part of a
    // (static) solid boundary. Templated on problem dimension
    template <unsigned N>
    class BoundaryND
    {
        protected:
            // Collection of boundary particles
            std::set<ParticleND<N>*> mp_particles;

        public:
            // Add a particle to the collection
            void add(ParticleND<N>& particle);
            // Enable particles to interact with the fluid
            void start_interaction();
            // Make particles non-interactive, intended for use as tracers or
            // trackers of flow properties
            void stop_interaction();
            // Remove a particle from the boundary (making it a part of the
            // bulk fluid)
            void remove_all();
    };

#include <boundary.impl>

    // Aliases for up to three dimensions
    using Boundary1D = BoundaryND<1>;
    using Boundary2D = BoundaryND<2>;
    using Boundary3D = BoundaryND<3>;
}   // namespace SPH

#endif  // BOUNDARY_HPP

