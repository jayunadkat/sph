// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef DOMAIN_HPP
#define DOMAIN_HPP

#include <ostream>
#include <random>
#include <string>

#include <boundary_manager.hpp>
#include <kernel_base.hpp>
#include <particle.hpp>
#include <particle_function.hpp>
#include <physical_constants.hpp>
#include <quad_tree.hpp>

#include <equation_of_state.hpp>

namespace SPH
{
    // Problem manager class, stores particles and boundary information, kernel
    // and equation of state objects, quad tree for the spatial partitioning of
    // the particles, and other computation-related members. Templated on
    // problem dimension, kernel, and equation of state
    template <unsigned N, typename K, typename P>
    class Domain
    {
        protected:
            // Particles and boundaries
            BoundaryManagerND<N> m_boundaries;
            std::vector<ParticleND<N>> m_particles;
            // Quad tree for spatial partitioning
            QuadTree<ParticleND<N>> m_tree;
            // Kernel and equation of state objects
            K m_kernel;
            P m_eos;

            // Representative particle separation
            double m_dp = 0.;
            // Number of time steps taken since last density filtering
            unsigned m_filter_counter = 0;
            // Flag for viscous/inviscid equation systems
            bool m_viscous = true;

            // Computes artificial viscosity contribution between two particles
            double artificial_viscosity(const ParticleND<N>& x,
                    const ParticleND<N>& y) const;
            // Computes time rates of change of flow quantities for each
            // particle (and stores them in each particle)
            void update_motion_info();
            // Shepard density filtering
            void apply_shepard_filter();

        public:
            // Particle and boundary access
            BoundaryManagerND<N>& boundaries();
            const std::vector<ParticleND<N>>& particles() const;
            std::vector<ParticleND<N>>& particles();
            const ParticleND<N>& closest_particle(
                    VectorND<N, double> position) const;
            // Set representative particle separation
            void set_dp(double dp);
            // Access kernel and equation of state objects
            K& kernel();
            P& eos();

            // Uses CFL condition to compute maximum time step
            double calculate_timestep() const;
            // Perform time step for each particle
            void step(double dt);

            // Stream output (dumps boundary particle data and then fluid
            // particle data in separate blocks, in gnuplot format)
            friend std::ostream& operator<<(std::ostream& out,
                    const Domain<N, K, P>& rhs)
            {
                for (auto&& x : rhs.m_particles)
                    if (x.pinned())
                        out << x << std::endl;
                out << std::endl << std::endl;
                
                for (auto&& x : rhs.m_particles)
                    if (!x.pinned())
                        out << x << std::endl;
                out << std::endl << std::endl;

                return out;
            }
    };

    // Template specialisation for two dimensions
    template <typename K, typename P>
    class Domain2D : public Domain<2, K, P>
    {
        public:
            // Add an axis-aligned uniform box of particles with the given (SW
            // and NE) corners, with the given number of particles in each
            // direction, with a given local density. Optionally specify a name
            // for the box to add these particles to the boundary if it exists,
            // or create the boundary if it does not
            void add_box(Vector2f corner1, Vector2f corner2,
                    Vector2u resolution, const ScalarFunction2D& density,
                    const std::string& boundary = "");
            void add_box(Vector2f corner1, Vector2f corner2,
                    Vector2u resolution, const ScalarFunction2D& density,
                    const ScalarFunction2D& temperature,
                    const std::string& boundary = "");
            // Add an axis-aligned box of (uniformly) randomly distributed
            // particles with the given (SW and NE) corners, with the given
            // number of particles and a given local density. Optionally specify
            // a name for the box to add these particles to the boundary if it
            // exists, or create the boundary if it does not
            void add_box(Vector2f corner1, Vector2f corner2,
                    unsigned num_particles, const ScalarFunction2D& density,
                    const std::string& boundary = "");
            void add_box(Vector2f corner1, Vector2f corner2,
                    unsigned num_particles, const ScalarFunction2D& density,
                    const ScalarFunction2D& temperature,
                    const std::string& boundary = "");
    };

#include <domain.impl>
}   // namespace

#endif  // DOMAIN_HPP

