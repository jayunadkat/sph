// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef BOUNDARY_MANAGER_HPP
#define BOUNDARY_MANAGER_HPP

#include <map>
#include <string>

#include <boundary.hpp>

namespace SPH
{
    // Stores a collection of boundaries mapped to strings representing
    // descriptive names for each boundary. Templated on problem dimension
    template <unsigned N>
    class BoundaryManagerND
    {
        protected:
            // Collection of boundaries
            std::map<std::string, BoundaryND<N>> m_boundaries;

        public:
            // Add boundary
            void add(const std::string& id);
            // Access existing boundary
            BoundaryND<N>& operator[](const std::string& id);
    };

#include <boundary_manager.impl>

    // Aliases for up to three dimensions
    using BoundaryManager1D = BoundaryManagerND<1>;
    using BoundaryManager2D = BoundaryManagerND<2>;
    using BoundaryManager3D = BoundaryManagerND<3>;
}   // namespace SPH

#endif  // BOUNDARY_MANAGER_HPP

