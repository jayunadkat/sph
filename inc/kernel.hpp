// This file forms part of SPH, a simple project prototyping the solution of
// fluid dynamical problems via SPH.
//
// Copyright (C) 2017 Jay Unadkat
//
// This library is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this library.  If not, see <http://www.gnu.org/licenses/>.
//
// The author may be contacted at jdunadkat@gmail.com

#ifndef KERNEL_HPP
#define KERNEL_HPP

namespace SPH
{
    namespace Kernel
    {
        // Interface for Gaussian kernel, templated on problem dimension
        template <unsigned N>
        class GaussianND : public KernelND<N>
        {
            public:
                double W(const VectorND<N, double>& r) const;
                VectorND<N, double> GradW(const VectorND<N, double>& r) const;
        };

#include <kernel.impl>

        // Aliases for up to three dimensions
        using Gaussian1D = GaussianND<1>;
        using Gaussian2D = GaussianND<2>;
        using Gaussian3D = GaussianND<3>;
    }   // namespace Kernel
}   // namespace SPH

#endif  // KERNEL_HPP

