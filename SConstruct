# This file forms part of SPH, a simple project prototyping the solution of
# fluid dynamical problems via SPH.
#
# Copyright (C) 2017 Jay Unadkat
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#
# The author may be contacted at jay.unadkat@maths.man.ac.uk

import os.path
import glob

mainfile = ARGUMENTS.get('main', 'main')
optimise = ARGUMENTS.get('optimise', 0)
paranoid = ARGUMENTS.get('paranoid', 1)
profile = ARGUMENTS.get('profile', 0)
quiet = ARGUMENTS.get('quiet', 0)
sfml = ARGUMENTS.get('sfml', 0)

# Use colours
# <ESC>[{attr};{fg};{bg}m
red = "\033[1;31m"
yellow = "\033[1;33m"
green = "\033[1;32m"
blue = "\033[1;34m"
off = "\033[0m"

# default output format for messaging
def message(colour, text):
    print colour + "[build] " + text + off

comp_switches = '-Wall -std=c++14 -g '
preproc = ''
libs = ''
link_switches = ''
inc_dirs = 'inc '
lib_dirs = ''
build_dir = 'build'
main_src = mainfile + '.cpp'

if int(sfml):
    libs += 'sfml-graphics sfml-window sfml-system '
if int(optimise):
    comp_switches += '-O3 '
if int(paranoid):
    preproc += '-DPARANOID '
if int(quiet):
    preproc += '-DQUIET '
if int(profile):
    comp_switches += '-pg -no-pie '
    link_switches += '-pg -no-pie '

inc_dir_list = inc_dirs.split()
lib_dir_list = lib_dirs.split()

VariantDir(build_dir, '.', duplicate=0)
env = Environment(ENV = os.environ, FORTRAN = 'gfortran', CXX = 'g++', CPPPATH = inc_dir_list, CCFLAGS = comp_switches + preproc, LINKFLAGS = link_switches , LIBS = libs.split(), LIBPATH = lib_dir_list)

message(red, 'Building target')
env.Program(build_dir + '/' + main_src)

