#include <iostream>
#include <fstream>
#include <random>
#include <vector>

#include <boundary.hpp>
#include <boundary_manager.hpp>
#include <domain.hpp>
#include <equation_of_state.hpp>
#include <kernel_base.hpp>
#include <kernel.hpp>
#include <particle.hpp>
#include <particle_function.hpp>
#include <physical_constants.hpp>
#include <quad_tree.hpp>
#include <vector.hpp>

int test_quad_tree()
{
    std::random_device rd;
    std::mt19937 engine{rd()};
    std::uniform_real_distribution<double> dist{0., 1.};

    unsigned n(10);
    std::ofstream data("./dat/test");

    std::vector<SPH::Particle2D> particles(n*n);
    for (unsigned i(0); i < n; ++i)
        for (unsigned j(0); j < n; ++j)
            particles[n*j + i].r() = {dist(engine), dist(engine)};

    SPH::QuadTree<SPH::Particle2D> tree;
    tree.build(particles);

    for (auto&& x : particles)
        data << x << std::endl;
    data << std::endl << std::endl;
    data << tree << std::endl;

    data.close();
    return 0;
}

int rand()
{
    double boundary_right(0.5);
    double boundary_top(1.);
    unsigned num_boundary_x(50);
    unsigned boundary_width(3);
    double dp_boundary(boundary_right/num_boundary_x);
    unsigned num_boundary_y(boundary_top/dp_boundary);

    double fluid_left(0.15);
    double fluid_right(0.35);
    double fluid_bottom(0.);
    double fluid_top(SPH::Problem::fluid_height);
    unsigned num_x(20), num_y(20);
    double dx_fluid((fluid_right - fluid_left)/num_x);
    double dy_fluid((fluid_top - fluid_bottom)/num_y);

    SPH::ScalarFunction2D density = [](const SPH::ParticleData2D& data) -> double
    {
        return SPH::Problem::reference_density;
    };

    SPH::Domain2D<SPH::Kernel::Gaussian2D, SPH::EquationOfState::Tait2D> domain;
    domain.set_dp(std::max(dx_fluid, dy_fluid));
    domain.boundaries().add("left");
    domain.boundaries().add("right");
    domain.boundaries().add("bottom");

    domain.add_box(
            {-dp_boundary*boundary_width, -dp_boundary*boundary_width},
            {0., boundary_top},
            {boundary_width, num_boundary_y + boundary_width}, density,
            "left");
    domain.add_box(
            {0., -dp_boundary*boundary_width},
            {boundary_right, 0.},
            {num_boundary_x, boundary_width}, density,
            "bottom");
    domain.add_box(
            {boundary_right, -dp_boundary*boundary_width},
            {boundary_right + dp_boundary*boundary_width, boundary_top},
            {boundary_width, num_boundary_y + boundary_width}, density,
            "right");
    domain.add_box(
            {fluid_left, fluid_bottom},
            {fluid_right, fluid_top},
            {num_x, num_y},
            density);

    unsigned counter(0);
    char filename[100];

    std::sprintf(filename, "dat/time_dat/%05u.dat", counter);
    std::ofstream data(filename);
    data << domain;
    data.close();

    for (double t(0.), dt(0.0001), max_t(1.); t < max_t; t += dt, ++counter)
    {
        double max(0.);
        for (auto&& x : domain.particles())
            max = std::max(max, x.u().L2_norm());

        dt = domain.calculate_timestep();
        std::cout << "Starting t = " << (t + dt) << ", index = "
            << counter << ", dt = " << dt << ", max speed = " << max
            << std::endl;
        domain.step(dt);

        std::sprintf(filename, "dat/time_dat/%05u.dat", counter);
        data.open(filename);
        data << domain;
        data.close();
    }

    std::sprintf(filename, "dat/time_dat/animate.plt");
    data.open(filename);
    data << "set terminal png" << std::endl << "set yrange ["
        << -dp_boundary*(boundary_width + 1) << ":"
        << boundary_right + dp_boundary*(boundary_width + 1) << "]" << std::endl;
    data << "do for [i=0:" << counter << "] { "
        << "set output sprintf('\%05u.png', i); plot sprintf('\%05u.dat', i)"
        << " i 0 w p notitle, '' i 1 w p ti sprintf('t = %u', i)" << std::endl;
    data.close();

    std::sprintf(filename, "dat/time_dat/animate.sh");
    data.open(filename);
    data << "#! /bin/bash" << std::endl << "set -x" << std::endl;
    data << "gnuplot animate.plt" << std::endl
        << "ffmpeg -r 60 -i %05d.png -vcodec libx264 -crf 25 0.mp4"
        << std::endl << "rm ./*.png" << std::endl;

    return 0;
}

int rand2()
{
    double dp(0.0025);
    double left(0.), right(0.2), bottom(0.), top(0.05);
    unsigned num_x((right - left)/dp), num_y((top - bottom)/dp);

    SPH::ScalarFunction2D density = [](const SPH::ParticleData2D& data) -> double
    {
        return SPH::Problem::reference_density;
    };

    SPH::ScalarFunction2D linear = [](const SPH::ParticleData2D& data) -> double
    {
        return 300. + 5.*data.position[0];
    };

    SPH::ScalarFunction2D cold = [](const SPH::ParticleData2D& data) -> double
    {
        return 273.;
    };

    SPH::ScalarFunction2D hot = [](const SPH::ParticleData2D& data) -> double
    {
        return 373.;
    };

    SPH::Domain2D<SPH::Kernel::Gaussian2D, SPH::EquationOfState::Tait2D> domain;
    domain.set_dp(dp);
//    domain.boundaries().add("bar");
//
//    domain.add_box(
//            {left, bottom}, {right, top}, {num_x, num_y}, density, linear,
//            "bar");

    domain.boundaries().add("cold_bar");
    domain.boundaries().add("hot_bar");

    domain.add_box(
            {0., 0.}, {0.1, 0.05}, {num_x/2, num_y}, density, hot, "hot_bar");
    domain.add_box(
            {0.1, 0.}, {0.2, 0.05}, {num_x/2, num_y}, density, cold, "cold_bar");

    unsigned counter(0);
    char filename[100];

    std::sprintf(filename, "dat/time_dat/%05u.csv", counter++);
    std::ofstream data(filename);
    data << domain;
    data.close();

    for (double t(0.), dt(0.0001), max_t(0.001), dt_out(0.01), next_out(dt_out); t < max_t; t += dt)
    {
        if (t > next_out)
        {
            std::sprintf(filename, "dat/time_dat/%05u.csv", counter++);
            data.open(filename);
            data << domain;
            data.close();

            next_out += dt_out;
        }

        double max(0.);
        for (auto&& x : domain.particles())
            max = std::max(max, x.u().L2_norm());

        dt = domain.calculate_timestep();
        std::cout << "Starting t = " << (t + dt) << ", dt = " << dt << ", max speed = " << max
            << std::endl;
        domain.step(dt);
    }

    std::cout << "Files output: " << counter << std::endl;
    return 0;
}

int main(int argc, char** argv)
{
    return rand2();
}

