#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>

#include <SFML/Graphics.hpp>

#include <domain.hpp>
#include <equation_of_state.hpp>
#include <kernel.hpp>
#include <particle.hpp>
#include <physical_constants.hpp>

class ParticleRenderer
{
    protected:
        const sf::Vector2u m_resolution = {800, 600};
        sf::RenderWindow m_window;
        const std::vector<SPH::Particle2D>& m_particles;

        const sf::Color m_boundary_colour;
        const sf::Color m_fluid_colour;
        sf::Font m_font;
        sf::Text m_text;

        const double m_min_left = -0.2, m_min_right = 1.2;
        const double m_min_top = 1.2, m_min_bottom = -0.2;
        double m_left, m_right, m_top, m_bottom;

        void update_bounds()
        {
            m_left = m_min_right;
            m_right = m_min_left;
            m_top = m_min_bottom;
            m_bottom = m_min_top;

            for (auto&& p : m_particles)
            {
                if (m_left > p.r()[0])
                    m_left = p.r()[0];
                if (m_right < p.r()[0])
                    m_right = p.r()[0];
                if (m_bottom > p.r()[1])
                    m_bottom = p.r()[1];
                if (m_top < p.r()[1])
                    m_top = p.r()[1];
            }

            m_left = std::min(m_left, m_min_left);
            m_right = std::max(m_right, m_min_right);
            m_bottom = std::min(m_bottom, m_min_bottom);
            m_top = std::max(m_top, m_min_top);
        }

        sf::Vector2u to_screen(const SPH::Vector2f& r)
        {
            sf::Vector2u temp;
            temp.x = m_resolution.x*(r[0] - m_left)/(m_right - m_left);
            temp.y = m_resolution.y*(m_top - r[1])/(m_top - m_bottom);
            return temp;
        }

        void draw_particle(const SPH::Particle2D& p)
        {
            sf::CircleShape shape(2);
            sf::Vector2u pos(to_screen(p.r()));
            shape.setPosition(pos.x, pos.y);
            if (p.pinned())
                shape.setFillColor(m_boundary_colour);
            else
                shape.setFillColor(m_fluid_colour);
            m_window.draw(shape);
        }

    public:
        ParticleRenderer(const std::vector<SPH::Particle2D>& particles) :
            m_window(sf::VideoMode(m_resolution.x, m_resolution.y), "SPH"),
            m_particles(particles),
            m_boundary_colour(200, 50, 50),
            m_fluid_colour(50, 50, 200)
        {
            m_window.setFramerateLimit(30);

            if (!m_font.loadFromFile(
                        "/usr/share/fonts/truetype/freefont/FreeSans.ttf"))
                throw std::runtime_error("Invalid font");
            m_text.setFont(m_font);
            m_text.setCharacterSize(16);
            m_text.setColor(sf::Color::Red);
            m_text.setStyle(sf::Text::Bold);
        }

        sf::RenderWindow& window()
        {
            return m_window;
        }

        void update(double t)
        {
            std::stringstream ss;
            ss << "t = " << t;

            update_bounds();
            m_text.setString(ss.str());
            m_window.clear();
            for (auto&& p : m_particles)
                draw_particle(p);
            m_window.draw(m_text);
            m_window.display();
        }
};

int main()
{
    double boundary_right(1.);
    double boundary_top(1.);
    unsigned num_boundary_x(20);
    unsigned boundary_width(2);
    double dp_boundary(boundary_right/num_boundary_x);
    unsigned num_boundary_y(boundary_top/dp_boundary);

    double fluid_left(0.);
    double fluid_right(0.5);
    double fluid_bottom(0.);
    double fluid_top(SPH::Problem::fluid_height);
    unsigned num_x((fluid_right - fluid_left)/dp_boundary),
             num_y((fluid_top - fluid_bottom)/dp_boundary);
    double dx_fluid(dp_boundary);
    double dy_fluid(dp_boundary);

    SPH::ScalarFunction2D density = [](const SPH::ParticleData2D& data) -> double
    {
        return SPH::Problem::reference_density;
    };

    SPH::Domain2D<SPH::Kernel::Gaussian2D, SPH::EquationOfState::Tait2D> domain;
    ParticleRenderer render(domain.particles());
    domain.set_dp(std::max(dx_fluid, dy_fluid));
    domain.boundaries().add("left");
    domain.boundaries().add("right");
    domain.boundaries().add("bottom");

    domain.add_box(
            {-dp_boundary*boundary_width, -dp_boundary*boundary_width},
            {0., boundary_top},
            {boundary_width, num_boundary_y + boundary_width}, density,
            "left");
    domain.add_box(
            {0., -dp_boundary*boundary_width},
            {boundary_right, 0.},
            {num_boundary_x, boundary_width}, density,
            "bottom");
    domain.add_box(
            {boundary_right, -dp_boundary*boundary_width},
            {boundary_right + dp_boundary*boundary_width, boundary_top},
            {boundary_width, num_boundary_y + boundary_width}, density,
            "right");
    domain.add_box(
            {fluid_left, fluid_bottom},
            {fluid_right, fluid_top},
            {num_x, num_y},
            density);

    double t(0.), dt(0.0001), max_t(1.);
    render.update(t);
    while (render.window().isOpen() && t < max_t)
    {
        sf::Event event;
        while (render.window().pollEvent(event))
        {
            switch (event.type)
            {
                case sf::Event::Closed:
                    render.window().close();
                    break;
                default:
                    break;
            }
        }

        dt = domain.calculate_timestep();
        domain.step(dt);
        t += dt;
        render.update(t);
    }

    return 0;
}

